<?php

/**
 * @file
 * Contains \Drupal\disc\Controller\DiscBatchController.
 */

namespace Drupal\layout_builder_content_sync_fixer\Controller;

class ProcessBatchController {
  
  public function content() {

    $database = \Drupal::database();
    $query = $database->query("SELECT nid, type FROM node");
    $results = $query->fetchAll();

    $operations = [];

    foreach ($results as $node) {
      $operations[] = [
        [get_called_class(), 'process'], ['unused', ['nid' => $node->nid, 'type' => $node->type]]
      ];
    }

    $batch = [
      'title' => t('Exporting'),
      'operations' => $operations,
      'finished' => [get_called_class(), 'finished_callback'],
    ];

    batch_set($batch);
    return batch_process('user');
  }

  public static function finished_callback($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One post processed.', '@count posts processed.'
      );
    } else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addMessage($message);
  }

  public static function process($type, $options = [], &$context) {
    
    $database = \Drupal::database();

    $ndata = \Drupal::service('entity_type.manager')->getStorage('node')->load($options['nid']);

    $layout_query = $database->query("SELECT revision_id, langcode, delta, CAST(layout_builder__layout_section AS CHAR(200000) CHARACTER SET utf8) AS layout  FROM {node__layout_builder__layout} WHERE entity_id = {$options['nid']} AND bundle = '{$options['type']}' AND deleted = 0");
    
    $sections = $layout_query->fetchAll();
    foreach ($sections as $section) {
      $unserialized = unserialize($section->layout);
      $modified = FALSE;

      if ($unserialized instanceof \Drupal\layout_builder\Section) {
        $components = $unserialized->getComponents();
        foreach ($components as &$component) {
          $unserialized->removeComponent($component->getUuid());
          $plugin = $component->getPlugin();
          if ($plugin instanceof \Drupal\layout_builder\Plugin\Block\InlineBlock) {
            $config = $plugin->getConfiguration();
            if (isset($config['uuid']) && isset($config['type']) && isset($config['block_revision_id'])) {
              $block = \Drupal::service('entity.repository')->loadEntityByUuid('block_content', $config['uuid']);
              if ($block != NULL && $block->bundle() == $config['type'] && $block->getRevisionId() != $config['block_revision_id']) {
                $config['block_revision_id'] = $block->getRevisionId();
                $component->setConfiguration($config);
                $modified = TRUE;
              }
            }
          }
        }

        foreach ($components as &$component) {
          $unserialized->insertComponent(0, $component);
        }
      }

      $section->layout = serialize($unserialized);
      if ($modified) {
        $database->update('node__layout_builder__layout')
          ->fields([
            'layout_builder__layout_section' => $section->layout,
          ])
          ->condition('entity_id', $options['nid'], '=')
          ->condition('bundle', $options['type'], '=')
          ->condition('deleted', 0, '=')
          ->condition('revision_id', $section->revision_id, '=')
          ->condition('langcode', $section->langcode, '=')
          ->condition('delta', $section->delta, '=')
          ->execute();
      }
    }
  }
}