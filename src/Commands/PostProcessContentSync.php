<?php

namespace Drupal\layout_builder_content_sync_fixer\Commands;

use Drush\Commands\DrushCommands;
use Drupal\layout_builder_content_sync_fixer\Controller\ProcessBatchController;

class PostProcessContentSync extends DrushCommands {

    /**
     * Drush command to fix Content Sync for Layout Builder
     * 
     * @command content-sync-fixer:all
     * @aliases csff
     */

    public function all() {
        $database = \Drupal::database();
        $query = $database->query("SELECT nid, type FROM node");
        $results = $query->fetchAll();

        $operations = [];

        foreach ($results as $node) {
        $operations[] = [
            [ProcessBatchController::class, 'process'], ['unused', ['nid' => $node->nid, 'type' => $node->type]]
        ];
        }

        $batch = [
            'title' => t('Exporting'),
            'operations' => $operations,
            'finished' => [ProcessBatchController::class, 'finished_callback'],
        ];

        batch_set($batch);
        drush_backend_batch_process();
        $this->io()->writeln('Batch operations end.');
    }
}

